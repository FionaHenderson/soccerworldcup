﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SoccerWorldCup
{
    public partial class dataGridViewTeams : Form
    {
        public dataGridViewTeams()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'teamsDataSet.teams' table. You can move, or remove it, as needed.
            this.teamsTableAdapter.Fill(this.teamsDataSet.teams);           
        }
       
        private void updateTeams_Click_1(object sender, EventArgs e)
        {
            teamsTableAdapter.Update(teamsDataSet.teams);
        }
    }
}
