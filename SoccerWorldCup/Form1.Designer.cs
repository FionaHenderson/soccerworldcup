﻿namespace SoccerWorldCup
{
    partial class dataGridViewTeams
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.teamsDataSet = new SoccerWorldCup.TeamsDataSet();
            this.teamsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.teamsTableAdapter = new SoccerWorldCup.TeamsDataSetTableAdapters.teamsTableAdapter();
            this.teamnameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.teammatchesplayedDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.goalsforDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.goalsagainstDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.goaldifferenceDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.teamshotsDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.teamshotsongoalDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.teamyellowcardsDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.teamredcardsDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.updateTeams = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teamsDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teamsBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.teamnameDataGridViewTextBoxColumn,
            this.teammatchesplayedDataGridViewTextBoxColumn,
            this.goalsforDataGridViewTextBoxColumn,
            this.goalsagainstDataGridViewTextBoxColumn,
            this.goaldifferenceDataGridViewTextBoxColumn,
            this.teamshotsDataGridViewTextBoxColumn,
            this.teamshotsongoalDataGridViewTextBoxColumn,
            this.teamyellowcardsDataGridViewTextBoxColumn,
            this.teamredcardsDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.teamsBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(0, 0);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(960, 370);
            this.dataGridView1.TabIndex = 0;
            // 
            // teamsDataSet
            // 
            this.teamsDataSet.DataSetName = "TeamsDataSet";
            this.teamsDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // teamsBindingSource
            // 
            this.teamsBindingSource.DataMember = "teams";
            this.teamsBindingSource.DataSource = this.teamsDataSet;
            // 
            // teamsTableAdapter
            // 
            this.teamsTableAdapter.ClearBeforeFill = true;
            // 
            // teamnameDataGridViewTextBoxColumn
            // 
            this.teamnameDataGridViewTextBoxColumn.DataPropertyName = "team_name";
            this.teamnameDataGridViewTextBoxColumn.HeaderText = "team_name";
            this.teamnameDataGridViewTextBoxColumn.Name = "teamnameDataGridViewTextBoxColumn";
            // 
            // teammatchesplayedDataGridViewTextBoxColumn
            // 
            this.teammatchesplayedDataGridViewTextBoxColumn.DataPropertyName = "team_matches_played";
            this.teammatchesplayedDataGridViewTextBoxColumn.HeaderText = "team_matches_played";
            this.teammatchesplayedDataGridViewTextBoxColumn.Name = "teammatchesplayedDataGridViewTextBoxColumn";
            // 
            // goalsforDataGridViewTextBoxColumn
            // 
            this.goalsforDataGridViewTextBoxColumn.DataPropertyName = "goals_for";
            this.goalsforDataGridViewTextBoxColumn.HeaderText = "goals_for";
            this.goalsforDataGridViewTextBoxColumn.Name = "goalsforDataGridViewTextBoxColumn";
            // 
            // goalsagainstDataGridViewTextBoxColumn
            // 
            this.goalsagainstDataGridViewTextBoxColumn.DataPropertyName = "goals_against";
            this.goalsagainstDataGridViewTextBoxColumn.HeaderText = "goals_against";
            this.goalsagainstDataGridViewTextBoxColumn.Name = "goalsagainstDataGridViewTextBoxColumn";
            // 
            // goaldifferenceDataGridViewTextBoxColumn
            // 
            this.goaldifferenceDataGridViewTextBoxColumn.DataPropertyName = "goal_difference";
            this.goaldifferenceDataGridViewTextBoxColumn.HeaderText = "goal_difference";
            this.goaldifferenceDataGridViewTextBoxColumn.Name = "goaldifferenceDataGridViewTextBoxColumn";
            // 
            // teamshotsDataGridViewTextBoxColumn
            // 
            this.teamshotsDataGridViewTextBoxColumn.DataPropertyName = "team_shots";
            this.teamshotsDataGridViewTextBoxColumn.HeaderText = "team_shots";
            this.teamshotsDataGridViewTextBoxColumn.Name = "teamshotsDataGridViewTextBoxColumn";
            // 
            // teamshotsongoalDataGridViewTextBoxColumn
            // 
            this.teamshotsongoalDataGridViewTextBoxColumn.DataPropertyName = "team_shots_on_goal";
            this.teamshotsongoalDataGridViewTextBoxColumn.HeaderText = "team_shots_on_goal";
            this.teamshotsongoalDataGridViewTextBoxColumn.Name = "teamshotsongoalDataGridViewTextBoxColumn";
            // 
            // teamyellowcardsDataGridViewTextBoxColumn
            // 
            this.teamyellowcardsDataGridViewTextBoxColumn.DataPropertyName = "team_yellow_cards";
            this.teamyellowcardsDataGridViewTextBoxColumn.HeaderText = "team_yellow_cards";
            this.teamyellowcardsDataGridViewTextBoxColumn.Name = "teamyellowcardsDataGridViewTextBoxColumn";
            // 
            // teamredcardsDataGridViewTextBoxColumn
            // 
            this.teamredcardsDataGridViewTextBoxColumn.DataPropertyName = "team_red_cards";
            this.teamredcardsDataGridViewTextBoxColumn.HeaderText = "team_red_cards";
            this.teamredcardsDataGridViewTextBoxColumn.Name = "teamredcardsDataGridViewTextBoxColumn";
            // 
            // updateTeams
            // 
            this.updateTeams.Location = new System.Drawing.Point(70, 391);
            this.updateTeams.Name = "updateTeams";
            this.updateTeams.Size = new System.Drawing.Size(75, 23);
            this.updateTeams.TabIndex = 1;
            this.updateTeams.Text = "Save";
            this.updateTeams.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.updateTeams.UseVisualStyleBackColor = true;
            this.updateTeams.Click += new System.EventHandler(this.updateTeams_Click_1);
            // 
            // dataGridViewTeams
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(971, 432);
            this.Controls.Add(this.updateTeams);
            this.Controls.Add(this.dataGridView1);
            this.Name = "dataGridViewTeams";
            this.Text = "Soccer World Cup - Teams";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teamsDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teamsBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private TeamsDataSet teamsDataSet;
        private System.Windows.Forms.BindingSource teamsBindingSource;
        private TeamsDataSetTableAdapters.teamsTableAdapter teamsTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn teamnameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn teammatchesplayedDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn goalsforDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn goalsagainstDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn goaldifferenceDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn teamshotsDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn teamshotsongoalDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn teamyellowcardsDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn teamredcardsDataGridViewTextBoxColumn;
        private System.Windows.Forms.Button updateTeams;
    }
}

